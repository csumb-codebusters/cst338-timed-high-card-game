import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

/**
 * CST 338 - Timed BUILD Card Game
 * <p>
 * GUI based BUILD card game using the deck of cards.
 *
 * @author Thomas Krause
 * @author Mercedes Garcia
 * @author Eric Ybarra
 * @author Willan Barajas
 */
public class Assig6 {

   /**
    * Main game execution thread.
    *
    * @param args
    */
   public static void main(String[] args) {
      Game game = new Game();
      game.start();
   }
}

/**
 * CST 338 - Timed BUILD Card Game
 * <p>
 * GUI based BUILD card game.
 *
 * @author Thomas Krause
 * @author Mercedes Garcia
 * @author Eric Ybarra
 * @author Willan Barajas
 */
class Game implements MouseListener, ActionListener {
   private static final int NUM_CARDS_PER_HAND = 7;
   private static final int NUM_PLAYERS = 2;

   private static GameEngine engine;
   private static GameUI ui;
   private static Timer timer;

   /**
    * Main game execution thread.
    */
   public void start() {
      int numPacksPerDeck = 1;
      int numJokersPerPack = 0;
      int numUnusedCardsPerPack = 0;
      Card[] unusedCardsPerPack = null;

      // Create and start timer
      timer = new Timer();
      timer.setOnTickListener(this);
      timer.start();

      // Create the engine / model
      engine = new GameEngine(numPacksPerDeck, numJokersPerPack,
         numUnusedCardsPerPack, unusedCardsPerPack,
         NUM_PLAYERS, NUM_CARDS_PER_HAND);

      // Create the UI / view
      ui = new GameUI(engine);

      engine.start();
      ui.start(this);
   }

   /**
    * Handler for timer tick.
    *
    * @param e
    */
   @Override
   public void actionPerformed(ActionEvent e) {
      // On timer tick event
      if (e.getActionCommand().equals("tick")) {
         Timer timer = (Timer) e.getSource();
         ui.onTick(timer.toString());
      }
   }

   /**
    * Relays the click event to the game engine and ui.
    *
    * @param e
    */
   @Override
   public void mouseClicked(MouseEvent e) {
      // Determine what they clicked
      JComponent clicked = (JComponent) e.getSource();
      String name = clicked.getName();

      // Timer toggle button was clicked
      if (name != null && name.equals("timerToggle")) {
         // Timer toggle was clicked
         timer.toggle();
         ui.onTimerToggle(timer.isRunning());

         // One of the pile cards or skip button clicked
      } else if (
         name != null && (name.contains("pile") || name.equals("skip"))) {
         // If it's a pile card and there is no selection
         // no go
         boolean skip = name.equals("skip");
         if (! skip && ! ui.hasSelection())
            return;

         Object prop = clicked.getClientProperty("pile");
         int pile = (int) (prop == null ? 0 : prop);
         int index = ui.getSelection();

         // Ignore event if the card cannot be played on the pile
         if (! skip && ! engine.canPlayCardOn(index, engine.HUMAN_ID, pile))
            return;

         // Take player turns
         engine.playerTurn(index, pile);
         // Update UI
         ui.onPlayCard(index);

         // If game over, show game over screen
         if (engine.isGameOver()) {
            timer.stop();
            ui.gameOver();
         }
      } else {
         ui.onSelectCard(clicked);
      }
   }

   /**
    * Hover state for cards.
    *
    * @param e
    */
   @Override
   public void mouseEntered(MouseEvent e) {
      // Ignore hover event on timer toggle
      if (!e.getComponent().getName().equals("card"))
         return;

      // If game is over, do nothing
      if (engine.isGameOver())
         return;

      JLabel cardLabel = (JLabel) e.getSource();
      ui.onHoverCard(cardLabel);
   }

   /**
    * Hover leave state for cards.
    *
    * @param e
    */
   @Override
   public void mouseExited(MouseEvent e) {
      // Ignore hover event on timer toggle
      if (!e.getComponent().getName().equals("card"))
         return;

      // If game is over, do nothing
      if (engine.isGameOver())
         return;

      JLabel cardLabel = (JLabel) e.getSource();
      ui.onLeaveCard(cardLabel);
   }

   @Override
   public void mousePressed(MouseEvent e) {
   }

   @Override
   public void mouseReleased(MouseEvent e) {
   }
}

/**
 * Card Game Engine for high card game.
 * <p>
 * Also known as a model in the MVC pattern.
 *
 * @author Thomas Krause
 * @version 1.0
 */
class GameEngine extends CardGameFramework {
   public static final int COMPUTER_ID = 0;
   public static final int HUMAN_ID = 1;
   public static final int NUM_PILES = 2;

   private int[] scores;
   private Card[] piles;
   private boolean[] playedThisTurn;

   public GameEngine(int numPacks, int numJokersPerPack,
                     int numUnusedCardsPerPack, Card[] unusedCardsPerPack,
                     int numPlayers, int numCardsPerHand
   ) {
      super(
         numPacks, numJokersPerPack,
         numUnusedCardsPerPack, unusedCardsPerPack,
         numPlayers, numCardsPerHand
      );

      this.scores = new int[numPlayers];
      this.piles = new Card[NUM_PILES];
   }

   /**
    * Starts the game engine. In our case, we just deal
    * and sort the dealt cards.
    */
   public void start() {
      this.deal();
      this.sortHands();
      this.dealCenterCards();
   }

   /**
    * Returns the index of the first pile that the card can be played on.
    * Negative number means the card cannot be played.
    *
    * @param k
    * @param player
    * @return pileIndex
    */
   public int canPlayCard(int k, int player) {
      for (int i = 0; i < this.piles.length; i++) {
         if (this.canPlayCardOn(k, player, i)) {
            return i;
         }
      }

      return -1;
   }

   /**
    * Determines if a card for a player can be played on a specific
    * pile.
    *
    * @param k      card
    * @param player player
    * @param pile   pile
    * @return canBePlayed
    */
   public boolean canPlayCardOn(int k, int player, int pile) {
      Card card = this.getHand(player).inspectCard(k);
      // If card is not valid, cannot be played
      if (card == null || card.getErrorFlag())
         return false;

      // No such pile exists
      if (pile > this.piles.length || pile < 0)
         return false;

      Card onPile = this.piles[pile];

      // If the difference is exactly one
      return Math.abs(onPile.valueAsInt() - card.valueAsInt()) == 1;
   }

   private Card playCard(int card, int player, int pile) {
      // If they can play the card on the pile
      // attempt to get the card
      if (this.canPlayCardOn(card, player, pile)) {
         return this.playCard(player, card);
      }

      // Bad card if they cannot play
      return new Card(true);
   }


   /**
    * Player takes their turn.
    *
    * @param card
    */
   public void playerTurn(int card, int pile) {
      // Reset the variable keeping track of who has played.
      int players = this.getNumPlayers();
      this.playedThisTurn = new boolean[players];

      // Since there are only two players assumed
      // for this game we can just loop backwards so the cpu
      // is last.
      boolean stuck = true;
      for (int i = players - 1; i >= 0; i--) {
         Card played = COMPUTER_ID == i ?
            this.computerTurn() : this.playCard(card, i, pile);

         // If the card is valid, add it to the pile
         if (played != null && !played.getErrorFlag()) {
            stuck = false;
            this.piles[pile] = played;
            this.playedThisTurn[i] = true;
            this.takeCard(i);
         }
      }

      // If someone was able to go, update the states
      if (!stuck) {
         // Re-order the hands
         this.sortHands();
         // Compute the player score for the round
         this.computeScore();
      } else {
         // Both players are stuck, deal center cards
         this.dealCenterCards();
      }
   }

   /**
    * Computes which card the computer should play.
    *
    * @return card
    */
   private Card computerTurn() {
      // Loop through our cards until we find one
      // we can play.
      Hand cpuHand = this.getHand(COMPUTER_ID);
      for (int i = 0; i < cpuHand.getNumCards(); i++) {
         int pile = this.canPlayCard(i, COMPUTER_ID);
         // This card does not work, keep trying
         if (pile < 0)
            continue;

         // Play the card on the pile we found
         Card playing = cpuHand.playCard(i);
         this.piles[pile] = playing;

         return playing;
      }

      // Could not play anything, skipping...
      return new Card(true);
   }

   /**
    * Deals the cards to the center deck piles.
    *
    * @return success
    */
   private boolean dealCenterCards() {
      if (this.getNumCardsRemainingInDeck() < NUM_PILES)
         return false;

      for (int i = 0; i < NUM_PILES; i++)
         this.piles[i] = this.deck.dealCard();

      return true;
   }

   /**
    * Determine if the game is over or not.
    *
    * @return gameOver
    */
   public boolean isGameOver() {
      // You'd actually want to check if all players are stuck
      // or someone is out of cards
      return this.deck.getNumCards() <= 0;
   }

   /**
    * Get the card from a given pile.
    *
    * @param pile
    * @return card
    */
   public Card getPileCard(int pile) {
      if (pile < 0 || pile > NUM_PILES)
         return new Card(true);

      return this.piles[pile];
   }

   /**
    * Gets the number of piles.
    *
    * @return piles
    */
   public int getNumPiles() {
      return NUM_PILES;
   }

   /**
    * Computes the scores for the round.
    */
   private void computeScore() {
      for (int i = 0; i < this.numPlayers; i++) {
         // The player went this turn, give them a point
         if (this.playedThisTurn[i])
            this.scores[i]++;
      }
   }

   /**
    * Gets the score for a given user id.
    *
    * @param id
    * @return score
    */
   public int getScore(int id) {
      if (id < 0 || id > this.numPlayers)
         return -1;

      return this.scores[id];
   }

   /**
    * The first player to the highest score wins.
    *
    * @return winnerId
    */
   public int getWinner() {
      // If the game isn't over we don't know who won
      if (!this.isGameOver()) {
         return -1;
      }

      int winner = 0;
      int max = 0;
      // Find the player with the highest score.
      for (int i = 0; i < this.numPlayers; i++) {
         if (this.scores[i] > max) {
            max = this.scores[i];
            winner = i;
         }
      }

      return winner;
   }

   /**
    * Determines if a given id is a CPU.
    *
    * @param player
    * @return
    */
   public boolean isCpu(int player) {
      return player == COMPUTER_ID;
   }

   /**
    * Determine if a given player is a human.
    *
    * @param player
    * @return
    */
   public boolean isHuman(int player) {
      return !this.isCpu(player);
   }

   /**
    * Determines if a player was skipped.
    *
    * @param id
    * @return card
    */
   public boolean isSkipped(int id) {
      if (id >= 0 && id < this.getNumPlayers()) {
         // Invert due to the naming scheme
         return !this.playedThisTurn[id];
      }

      // Error card if no player found with this ID.
      return true;
   }

   /**
    * Get the number of players in the game.
    *
    * @return numPlayers
    */
   public int getNumPlayers() {
      return this.numPlayers;
   }

   /**
    * Get the number of card in each player's hand.
    *
    * @return cardPerHand
    */
   public int getNumCardsPerHand() {
      return this.numCardsPerHand;
   }

}

// Card game framework
class CardGameFramework {
   protected static final int MAX_PLAYERS = 50;

   protected int numPlayers;
   protected int numPacks;
   protected int numJokersPerPack;
   protected int numUnusedCardsPerPack;
   protected int numCardsPerHand;
   protected Deck deck;
   // smaller (usually) during play
   protected Hand[] hand;
   protected Card[] unusedCardsPerPack;
   // in the game.  e.g. pinochle does not
   // use cards 2-8 of any suit

   public CardGameFramework(int numPacks, int numJokersPerPack,
                            int numUnusedCardsPerPack,
                            Card[] unusedCardsPerPack,
                            int numPlayers, int numCardsPerHand) {
      int k;

      // filter bad values
      if (numPacks < 1 || numPacks > 6)
         numPacks = 1;
      if (numJokersPerPack < 0 || numJokersPerPack > 4)
         numJokersPerPack = 0;
      if (numUnusedCardsPerPack < 0 || numUnusedCardsPerPack > 50)
         numUnusedCardsPerPack = 0;
      if (numPlayers < 1 || numPlayers > MAX_PLAYERS)
         numPlayers = 4;
      // one of many ways to assure at least one full deal to all players
      if (numCardsPerHand < 1 ||
         numCardsPerHand > numPacks * (52 - numUnusedCardsPerPack)
            / numPlayers)
         numCardsPerHand = numPacks *
            (52 - numUnusedCardsPerPack) / numPlayers;

      // allocate
      this.unusedCardsPerPack = new Card[numUnusedCardsPerPack];
      this.hand = new Hand[numPlayers];
      for (k = 0; k < numPlayers; k++)
         this.hand[k] = new Hand();
      deck = new Deck(numPacks);

      // assign to members
      this.numPacks = numPacks;
      this.numJokersPerPack = numJokersPerPack;
      this.numUnusedCardsPerPack = numUnusedCardsPerPack;
      this.numPlayers = numPlayers;
      this.numCardsPerHand = numCardsPerHand;
      for (k = 0; k < numUnusedCardsPerPack; k++)
         this.unusedCardsPerPack[k] = unusedCardsPerPack[k];

      // prepare deck and shuffle
      newGame();
   }

   // constructor overload/default for game like bridge
   public CardGameFramework() {
      this(1, 0, 0, null, 4, 13);
   }

   public Hand getHand(int k) {
      // hands start from 0 like arrays

      // on error return automatic empty hand
      if (k < 0 || k >= numPlayers)
         return new Hand();

      return hand[k];
   }

   public Card getCardFromDeck() {
      return deck.dealCard();
   }

   public int getNumCardsRemainingInDeck() {
      return deck.getNumCards();
   }

   public void newGame() {
      int k, j;

      // clear the hands
      for (k = 0; k < numPlayers; k++)
         hand[k].resetHand();

      // restock the deck
      deck.init(numPacks);

      // remove unused cards
      for (k = 0; k < numUnusedCardsPerPack; k++)
         deck.removeCard(unusedCardsPerPack[k]);

      // add jokers
      for (k = 0; k < numPacks; k++)
         for (j = 0; j < numJokersPerPack; j++)
            deck.addCard(new Card('X', Card.Suit.values()[j]));

      // shuffle the cards
      deck.shuffle();
   }

   public boolean deal() {
      // returns false if not enough cards, but deals what it can
      int k, j;
      boolean enoughCards;

      // clear all hands
      for (j = 0; j < numPlayers; j++)
         hand[j].resetHand();

      enoughCards = true;
      for (k = 0; k < numCardsPerHand && enoughCards; k++) {
         for (j = 0; j < numPlayers; j++)
            if (deck.getNumCards() > 0)
               hand[j].takeCard(deck.dealCard());
            else {
               enoughCards = false;
               break;
            }
      }

      return enoughCards;
   }

   void sortHands() {
      int k;

      for (k = 0; k < numPlayers; k++)
         hand[k].sort();
   }

   Card playCard(int playerIndex, int cardIndex) {
      // returns bad card if either argument is bad
      if (playerIndex < 0 || playerIndex > numPlayers - 1 ||
         cardIndex < 0 || cardIndex > numCardsPerHand - 1) {
         //Creates a card that does not work
         return new Card('M', Card.Suit.spades);
      }

      // return the card played
      return hand[playerIndex].playCard(cardIndex);
   }


   boolean takeCard(int playerIndex) {
      // returns false if either argument is bad
      if (playerIndex < 0 || playerIndex > numPlayers - 1)
         return false;

      // Are there enough Cards?
      if (deck.getNumCards() <= 0)
         return false;

      return hand[playerIndex].takeCard(deck.dealCard());
   }

}

/**
 * UI for the Card Game
 *
 * @author Thomas Krause
 * @version 1.1
 */
class GameUI {
   private JLabel[] computerLabels;
   private JLabel[] humanLabels;
   private JLabel[] cardPileLabels;
   private JLabel[] playLabelText;

   private JLabel timerLabel;
   private JLabel cardsInDeckLabel;
   private JButton timerToggle;

   private CardTable cardTable;
   private GameEngine engine;

   private MouseListener controller;
   private int selected = -1;

   /**
    * Create a UI with a model to render data later.
    *
    * @param game
    */
   public GameUI(GameEngine game) {
      this.engine = game;
   }

   /**
    * Boot up the actual UI
    */
   public void start(MouseListener controller) {
      this.controller = controller;

      // Initialize UI labels
      int cardsPerHand = engine.getNumCardsPerHand();
      int players = engine.getNumPlayers();

      computerLabels = new JLabel[cardsPerHand];
      humanLabels = new JLabel[cardsPerHand];
      cardPileLabels = new JLabel[players];
      playLabelText = new JLabel[players];

      // Pre-load card icons
      GUICard.loadCardIcons();

      // Setup main game window
      cardTable = new CardTable("BUILD");
      // Hide the window while it loads
      cardTable.setVisible(false);
      cardTable.setSize(800, 600);
      cardTable.setLocationRelativeTo(null);
      cardTable.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      // Create some bogus played cards and hide them
      // will be replaced by actual cards later
      for (int i = 0; i < players; i++) {
         JLabel pileLabel = new JLabel(GUICard.getIcon(new Card()));
         pileLabel.addMouseListener(this.controller);
         pileLabel.setName("pile" + i);
         pileLabel.putClientProperty("pile", i);
         cardPileLabels[i] = pileLabel;
      }

      playLabelText[GameEngine.COMPUTER_ID] = new JLabel(
         "", JLabel.CENTER
      );
      playLabelText[GameEngine.HUMAN_ID] = new JLabel(
         "", JLabel.CENTER
      );

      for (int i = 0; i < players; i++)
         cardTable.pnlPlayArea.add(cardPileLabels[i]);

      for (int i = 0; i < players; i++)
         cardTable.pnlPlayArea.add(playLabelText[i]);

      // Info labels on the right side
      timerLabel = new JLabel("0:00", JLabel.CENTER);
      cardTable.pnlInfoArea.add(new JLabel("Elapsed"));
      cardTable.pnlInfoArea.add(timerLabel);

      cardTable.pnlInfoArea.add(new JLabel("In Deck"));
      cardsInDeckLabel = new JLabel();
      cardTable.pnlInfoArea.add(cardsInDeckLabel);

      timerToggle = new JButton("Stop Timer");
      timerToggle.setName("timerToggle");
      timerToggle.addMouseListener(this.controller);
      cardTable.pnlInfoArea.add(timerToggle);

      JButton skipTurn = new JButton("I cannot play");
      skipTurn.addMouseListener(this.controller);
      skipTurn.putClientProperty("card", -1);
      skipTurn.setName("skip");
      cardTable.pnlInfoArea.add(skipTurn);

      // show everything to the user
      this.createLabelsForCards();

      // First render
      this.update();

      cardTable.validate();
      cardTable.setVisible(true);
   }

   /**
    * Show game over screen.
    */
   public void gameOver() {
      // If the game is not really over
      // don't show a game over message
      if (!engine.isGameOver())
         return;

      cardTable.dispose();

      // Get the message to show to the user
      String message = getGameOverMessage();
      // Show the dialog box
      JOptionPane.showMessageDialog(
         null, String.format(
            "%s\nComputer: %s, Human: %s",
            message,
            engine.getScore(GameEngine.COMPUTER_ID),
            engine.getScore(GameEngine.HUMAN_ID)
         ), "Game Over!", JOptionPane.PLAIN_MESSAGE
      );

      // Quit the game
      System.exit(0);
   }

   /**
    * When a card is played...
    *
    * @param index
    */
   public void onPlayCard(int index) {
      this.selected = -1;
      this.update();
   }

   /**
    * When a card is selected...
    *
    * @param cardLabel
    */
   public void onSelectCard(JComponent cardLabel) {
      // Ignore unplayable cards
      int index = (int) cardLabel.getClientProperty("card");

      // Card is selected, unselect it
      if (this.selected >= 0) {
         humanLabels[this.selected].setBorder(new EmptyBorder(2, 2, 2, 2));
      }

      // Unset if this card is already selected
      // or select the new card
      this.selected = this.selected == index ? -1 : index;

      // Update selection if there is one
      if (this.selected >= 0) {
         humanLabels[this.selected].setBorder(new LineBorder(Color.GREEN, 2));
      }
   }

   /**
    * When a card is hovered...
    *
    * @param cardLabel
    */
   public void onHoverCard(JComponent cardLabel) {
      // Ignore unplayable cards as well as the selected
      int playable = (int) cardLabel.getClientProperty("playable");
      int index = (int) cardLabel.getClientProperty("card");
      if (playable < 0 || this.selected == index)
         return;

      cardLabel.setBorder(new LineBorder(Color.BLUE, 2));
   }

   /**
    * When a card is no longer hovered...
    *
    * @param cardLabel
    */
   public void onLeaveCard(JComponent cardLabel) {
      // Ignore unplayable cards as well as the selected
      int playable = (int) cardLabel.getClientProperty("playable");
      int index = (int) cardLabel.getClientProperty("card");
      if (playable < 0 || this.selected == index)
         return;

      cardLabel.setBorder(new EmptyBorder(2, 2, 2, 2));
   }

   /**
    * When timer updates...
    *
    * @param time
    */
   public void onTick(String time) {
      // Timer label may not yet be initialized
      if (timerLabel != null)
         timerLabel.setText(time);
   }

   /**
    * When timer is paused and resumed...
    *
    * @param state
    */
   public void onTimerToggle(boolean state) {
      timerToggle.setText(state ? "Stop Timer" : "Start Timer");
   }

   /**
    * Gets the message to display to the user.
    *
    * @return message
    */
   private String getGameOverMessage() {
      int winner = engine.getWinner();
      // Did the computer win?
      if (GameEngine.COMPUTER_ID == winner) {
         return String.format("Sorry bro but you got wrecked!");
      } else {
         return "Nice job, you won!";
      }
   }

   /**
    * Gets the index of the selected card.
    *
    * @return card
    */
   public int getSelection() {
      return this.selected;
   }

   /**
    * Determines if there is a selection.
    *
    * @return
    */
   public boolean hasSelection() {
      return this.selected >= 0;
   }

   /**
    * Displays the UI representation of cards in the model.
    */
   private void displayHands() {
      int cardsPerHand = this.engine.getNumCardsPerHand();
      for (int i = 0; i < cardsPerHand; i++) {
         // Get the label for the player
         updateLabelForCard(GameEngine.HUMAN_ID, i);
         updateLabelForCard(GameEngine.COMPUTER_ID, i);
      }
   }

   /**
    * Creates the initial card labels.
    */
   private void createLabelsForCards() {
      int cardsPerHand = engine.getNumCardsPerHand();

      for (int i = 0; i < cardsPerHand; i++) {
         humanLabels[i] = new JLabel();
         humanLabels[i].addMouseListener(this.controller);
         humanLabels[i].setName("card");

         computerLabels[i] = new JLabel(GUICard.getBackCardIcon());

         cardTable.pnlHumanHand.add(humanLabels[i]);
         cardTable.pnlComputerHand.add(computerLabels[i]);
      }
   }

   /**
    * Update the labels to match the player hand.
    *
    * @param player
    * @param index
    */
   private void updateLabelForCard(int player, int index) {
      Card card = engine.getHand(player).inspectCard(index);
      JLabel label = engine.isCpu(player) ?
         computerLabels[index] : humanLabels[index];

      // Hide label is bad card
      label.setVisible(!Card.isError(card));
      if (engine.isHuman(player)) {
         int playable = engine.canPlayCard(index, player);
         label.setIcon(GUICard.getIcon(card));
         label.setEnabled(playable >= 0);
         label.setBorder(new EmptyBorder(2, 2, 2, 2));
         label.putClientProperty("card", index);
         label.putClientProperty("playable", playable);
      }
   }

   /**
    * Handles updating the UI.
    */
   private void update() {
      // Update the scores on the UI
      // the best thing to do would be to implement a Player class
      // but we're not going to do that for this project.
      for (int i = 0; i < engine.getNumPiles(); i++) {
         cardPileLabels[i].setIcon(GUICard.getIcon(
            engine.getPileCard(i)
         ));
      }

      for (int i = 0; i < engine.getNumPlayers(); i++) {
         playLabelText[i].setText(String.format("%s: %d",
            engine.isHuman(i) ? "You" : "Computer",
            engine.getScore(i)
         ));
      }

      // Update the icons for the cards in the hands
      this.displayHands();

      this.cardsInDeckLabel.setText(String.valueOf(
         engine.deck.getNumCards()
      ));

      // Repaint the UI
      cardTable.repaint();
   }
}

/**
 * Multi-threaded timer
 * <p>
 * Fires ActionListener on tick.
 *
 * @author Thomas Krause
 * @version 1.0
 */
class Timer extends Thread {
   private boolean running = false;
   private int seconds = 0;
   private ActionListener onTick;

   /**
    * On start set running to true.
    */
   @Override
   public synchronized void start() {
      running = true;

      super.start();
   }

   /**
    * Run the timer
    */
   @Override
   public void run() {
      // Loop forever and increment every tick while running
      while (true) {
         doNothing();

         if (running)
            this.increment();
      }
   }

   /**
    * Sleep the thread until next tick
    */
   public void doNothing() {
      try {
         Thread.sleep(1000);
      } catch (InterruptedException e) {

      }
   }

   /**
    * Toggle the state of the timer
    */
   public void toggle() {
      this.running = !this.running;
   }

   /**
    * Increment the timer value
    */
   private void increment() {
      this.setSeconds(seconds + 1);
   }

   /**
    * Set the second on the timer
    *
    * @param seconds
    */
   private void setSeconds(int seconds) {
      this.seconds = seconds;

      // Fire the tick listener
      if (this.onTick != null) {
         ActionEvent e = new ActionEvent(
            this, ActionEvent.ACTION_PERFORMED, "tick"
         );
         this.onTick.actionPerformed(e);
      }

   }

   /**
    * Set the listener that they want to trigger
    *
    * @param listener
    */
   public void setOnTickListener(ActionListener listener) {
      this.onTick = listener;
   }

   /**
    * Gets the state of the thread.
    *
    * @return
    */
   public boolean isRunning() {
      return running;
   }

   /**
    * Format the seconds on the timer for display
    *
    * @return
    */
   @Override
   public String toString() {
      int minutes = this.seconds / 60;
      int seconds = this.seconds % 60;

      return String.format("%d:%02d", minutes, seconds);
   }
}

/**
 * Custom frame to hold the game layout.
 *
 * @author Thomas Krause
 */
class CardTable extends JFrame {
   public JPanel pnlComputerHand, pnlHumanHand, pnlPlayArea, pnlInfoArea;

   public CardTable(String title) {
      super(title);

      // Create three panels to hold the cards
      pnlComputerHand = new JPanel();
      pnlHumanHand = new JPanel();
      pnlPlayArea = new JPanel(new GridLayout(2, 2));
      pnlInfoArea = new JPanel(new GridLayout(4, 2));

      // Display the titles on the panels
      pnlComputerHand.setBorder(new TitledBorder("Computer Hand"));
      pnlHumanHand.setBorder(new TitledBorder("Your Hand"));
      pnlPlayArea.setBorder(new TitledBorder("Playing Area"));
      // Right hand side panel
      pnlInfoArea.setBorder(new TitledBorder("Info"));

      setLayout(new BorderLayout(20, 10));

      add(pnlComputerHand, BorderLayout.NORTH);
      add(pnlPlayArea, BorderLayout.CENTER);
      add(pnlHumanHand, BorderLayout.SOUTH);
      add(pnlInfoArea, BorderLayout.EAST);
   }
}

/**
 * GUI Card representation with an icon.
 *
 * @author Thomas Krause
 */
class GUICard {
   private static Icon[][] iconCards = new ImageIcon[14][4];
   private static Icon iconBack;
   static boolean iconsLoaded = false;

   static char[] suits = {
      'C', 'D', 'H', 'S'
   };

   /**
    * Loads the icons for the cards if not already loaded.
    */
   static void loadCardIcons() {
      if (iconsLoaded)
         return;

      for (int s = 0; s < suits.length; s++) {
         char suit = suits[s];

         for (int i = 0; i < Card.valuRanks.length; i++) {
            char value = Card.valuRanks[i];
            String path = String.format("images/%s%s.gif", value, suit);
            iconCards[i][s] = new ImageIcon(path);
         }
      }

      iconBack = new ImageIcon("images/BK.gif");

      iconsLoaded = true;
   }

   /**
    * Gets an icon for a card.
    *
    * @param card
    * @return icon
    */
   public static Icon getIcon(Card card) {
      if (Card.isError(card))
         return null;

      loadCardIcons();

      return iconCards[card.valueAsInt()][card.suitAsInt()];
   }

   /**
    * Gets the icon for the back of the cards.
    *
    * @return
    */
   public static Icon getBackCardIcon() {
      return iconBack;
   }
}

/**
 * Card
 * CST 338 - M3 Deck of Cards
 *
 * @author Thomas Krause, Mercedes Garcia
 * @version 1.3
 */
class Card {
   public static final char[] validCardValues = {
      'A', '2', '3', '4', '5', '6', '7',
      '8', '9', 'T', 'J', 'Q', 'K', 'X'
   };

   public static char[] valuRanks = {
      '2', '3', '4', '5', '6', '7', '8',
      '9', 'T', 'J', 'Q', 'K', 'A', 'X'
   };

   public enum Suit {clubs, diamonds, hearts, spades}

   private char value;
   private Suit suit;
   private boolean errorFlag;

   /**
    * Default constructor for the card class
    */
   public Card() {
      this('A', Suit.spades);
   }

   /**
    * Constructor with all parameters.
    *
    * @param value
    * @param suit
    */
   public Card(char value, Suit suit) {
      this.set(value, suit);
   }

   /**
    * Invalid card setter.
    *
    * @param errorFlag
    */
   public Card(boolean errorFlag) {
      this.errorFlag = errorFlag;
   }

   /**
    * Copy constructor for card.
    *
    * @param card
    */
   public Card(Card card) {
      this.set(card.getValue(), card.getSuit());
   }

   //region Mutators

   /**
    * Set the card to a value and suit.
    *
    * @param value value
    * @param suit  suit
    * @return error
    */
   public boolean set(char value, Suit suit) {
      // If there is no error, set the values
      if (isValid(value, suit)) {
         this.value = value;
         this.suit = suit;

         this.errorFlag = false;
      } else {
         this.errorFlag = true;
      }

      return !this.errorFlag;
   }
   //endregion

   //region Accessors

   /**
    * Get the value of the card.
    *
    * @return value
    */
   public char getValue() {
      return value;
   }

   /**
    * Get the suit of the card.
    *
    * @return suit
    */
   public Suit getSuit() {
      return suit;
   }

   /**
    * Determine if the card has an error.
    *
    * @return error
    */
   public boolean getErrorFlag() {
      return errorFlag;
   }

   /**
    * Determines if the card is valid.
    *
    * @param value
    * @param suit
    * @return valid
    */
   public boolean isValid(char value, Suit suit) {
      for (char c : validCardValues) {
         if (value == c)
            return true;
      }

      return false;
   }

   /**
    * Helper method to swap places in an array.
    *
    * @param cards
    * @param a
    * @param b
    */
   static void swap(Card[] cards, int a, int b) {
      if (cards == null ||
         a < 0 || a >= cards.length ||
         b < 0 || b >= cards.length) {
         return;
      }

      Card tmp = cards[a];
      cards[a] = cards[b];
      cards[b] = tmp;
   }

   /**
    * Sorts a card array by suit and value.
    *
    * @param cards
    * @param arraySize
    */
   static void arraySort(Card[] cards, int arraySize) {
      int n = cards.length;
      for (int i = 0; i < n - 1; i++) {
         for (int j = i + 1; j < arraySize; j++) {
            if (cards[i].valueAsInt() > cards[j].valueAsInt()) {
               swap(cards, i, j);
            }
         }
      }
   }

   /**
    * Converts suit to an int.
    *
    * @return value
    */
   public int suitAsInt() {
      if (this.getErrorFlag())
         return -1;
      return this.getSuit().ordinal();
   }

   /**
    * Converts card to int between 0 and 14.
    *
    * @return value
    */
   public int valueAsInt() {
      if (this.getErrorFlag())
         return -1;

      char[] values = Card.valuRanks;
      for (int index = 0; index < values.length; index++)
         if (values[index] == this.getValue())
            return index;

      return -1;
   }

   //endregion

   /**
    * Outputs the value of the card in string form.
    *
    * @return string
    */
   public String toString() {
      if (errorFlag)
         return "** illegal **";

      return String.format("%s of %s", value, suit);
   }

   /**
    * Determines if two cards are equal.
    *
    * @param card otherCard
    * @return equal
    */
   public boolean equals(Card card) {
      return this.value == card.getValue()
         && this.suit == card.getSuit()
         && this.getErrorFlag() == card.getErrorFlag();
   }

   /**
    * Helper to determine if card is in error.
    *
    * @param card
    * @return
    */
   public static boolean isError(Card card) {
      return card == null || card.getErrorFlag();
   }
}

/**
 * Card Deck
 * CST 338 - M3 Deck of Cards
 *
 * @author Thomas Krause
 * @version 1.0
 */
class Deck {
   // Allow the deck to have up to 6 packs of cards
   public static final int NUMBER_OF_UNIQUE_CARDS = 56;
   public static final int MAX_CARDS = NUMBER_OF_UNIQUE_CARDS * 6;
   private static Card[] masterPack;

   private Card cards[] = new Card[MAX_CARDS];
   private int topCard = 0;
   private int numPacks;

   /**
    * Default constructor that creates one pack.
    */
   public Deck() {
      this(1);
   }

   /**
    * Creates a deck with up to 6 packs.
    *
    * @param numPacks number of packs
    */
   public Deck(int numPacks) {
      this.init(numPacks);
   }

   /**
    * Mutator that sets the number of packs.
    * If not in the range, we'll force it into the range.
    *
    * @param numPacks number of packs
    */
   private void setNumPacks(int numPacks) {
      if (numPacks > 6)
         numPacks = 6;

      if (numPacks < 0)
         numPacks = 1;

      this.numPacks = numPacks;
   }

   /**
    * Setup the master pack so that we can copy from it.
    */
   private static void allocateMasterPack() {
      if (masterPack != null)
         return;

      masterPack = new Card[NUMBER_OF_UNIQUE_CARDS];

      // For each suit, generate the cards
      int i = 0;
      for (Card.Suit suit : Card.Suit.values()) {
         // Thirteen cards in each suit
         for (char value : Card.validCardValues) {
            // Assign the card and move to the new card
            masterPack[i++] = new Card(value, suit);
         }
      }
   }

   /**
    * Initialize the deck with x packs.
    *
    * @param numPacks number of packs
    */
   public void init(int numPacks) {
      this.setNumPacks(numPacks);
      this.init();
   }

   /**
    * Initialize the deck with one pack.
    */
   public void init() {
      Deck.allocateMasterPack();

      topCard = 0;
      cards = new Card[MAX_CARDS];

      // Clone the cards from the masterPack
      int c = 0;
      for (int j = 0; j < numPacks; j++) {
         for (Card card : masterPack) {
            cards[c++] = card;
         }
      }
   }

   /**
    * Shuffle the deck.
    */
   public void shuffle() {
      Random rand = new Random();

      int numCards = numPacks * NUMBER_OF_UNIQUE_CARDS;
      for (int i = 0; i < numCards; i++) {
         // Random for remaining positions.
         int r = i + rand.nextInt(numCards - i);

         // Swap the elements
         Card temp = cards[r];
         cards[r] = cards[i];
         cards[i] = temp;

      }
   }

   /**
    * Adds a card to the deck.
    *
    * @param card
    * @return success
    */
   public boolean addCard(Card card) {
      // If we get a null card or we don't have room
      if (card == null || topCard >= numPacks * NUMBER_OF_UNIQUE_CARDS)
         return false;

      int found = 0;
      for (int i = 0; i < topCard; i++) {
         if (cards[i].equals(card))
            found++;
      }

      if (found >= numPacks)
         return false;

      cards[topCard] = new Card(card);
      topCard++;

      return true;
   }

   /**
    * Remove a specific card from the deck.
    *
    * @param card
    * @return success
    */
   public boolean removeCard(Card card) {
      if (card == null)
         return false;

      for (int i = 0; i < topCard; i++) {
         // We found a copy of the card
         if (cards[i].equals(card)) {
            // If the top card is the card we want, just
            // strip the top card
            if (cards[topCard].equals(card))
               dealCard();
               // Replace card with top card
            else {
               Card top = dealCard();
               cards[i] = top;
            }

            return true;
         }
      }

      return false;
   }

   /**
    * Deal a card from the deck
    *
    * @return card
    */
   public Card dealCard() {
      Card card = inspectCard(topCard);
      topCard++;

      return card;
   }

   /**
    * Retrieve the next card from the deck.
    *
    * @param k index
    * @return card
    */
   public Card inspectCard(int k) {
      // Are we out of index?
      if (k >= numPacks * NUMBER_OF_UNIQUE_CARDS)
         return new Card(true);

      // Has this card already been removed?
      Card card = cards[topCard];
      if (card == null)
         return new Card(true);

      return card;
   }

   /**
    * Get the index of the next card.
    *
    * @return topCard
    */
   public int getTopCard() {
      return topCard;
   }

   /**
    * Gets the number of cards that remain in the deck.
    *
    * @return numCardsLeft
    */
   public int getNumCards() {
      // Number of cards in the deck
      // minus the cards we've dealt
      return (numPacks * NUMBER_OF_UNIQUE_CARDS)
         - topCard;
   }

   /**
    * Sorts the cards by value.
    */
   public void sort() {
      Card.arraySort(cards, topCard);
   }
}

/**
 * Hand
 * CST 338 - M3 Deck of Cards
 *
 * @author Eric Ybarra, Willan Barajas, Thomas Krause
 * @version 1.1
 */
class Hand {
   // Constants
   public static final int MAX_CARDS = 50;

   // Private Data Members
   private Card[] myCards;
   private int numCards;

   /**
    * Default constructor.
    */
   public Hand() {
      resetHand();
   }

   /**
    * Resets the hand removing all cards.
    */
   public void resetHand() {
      myCards = new Card[MAX_CARDS];
      numCards = 0;
   }

   /**
    * Add a card to the hand.
    *
    * @param card card
    * @return error
    */
   public boolean takeCard(Card card) {
      if (numCards >= MAX_CARDS || card.getErrorFlag())
         return false;

      myCards[numCards++] = new Card(card);
      return true;
   }

   /**
    * Return and remove the top card
    *
    * @return card
    */
   public Card playCard() {
      Card card = inspectCard(numCards - 1);
      if (!card.getErrorFlag()) {
         myCards[--numCards] = null;
      }

      return card;
   }

   /**
    * Plays a card at a specific location.
    *
    * @param index
    * @return
    */
   public Card playCard(int index) {
      // If the card the top card
      if (index == numCards - 1)
         return this.playCard();

      Card card = inspectCard(index);
      if (!card.getErrorFlag()) {
         // Move the cards around to fill the void
         for (int i = index; i < numCards - 1; i++) {
            Card.swap(myCards, i, i + 1);
         }

         // Do the modification of the hand
         this.playCard();
      }

      return card;
   }

   /**
    * Gets the number of cards in the hand.
    *
    * @return numCards
    */
   public int getNumCards() {
      return numCards;
   }

   /**
    * Get a specified card from the hand.
    *
    * @param k index
    * @return card
    */
   public Card inspectCard(int k) {
      // If out of bounds, return error card.
      if (k > numCards || k < 0)
         return new Card(true);

      return myCards[k];
   }

   /**
    * Return the text representation of the hand.
    *
    * @return string
    */
   @Override
   public String toString() {
      String output = "Hand = ( ";

      for (int i = 0; i < numCards; i++) {
         output += String.format(
            "%s of %s, ", myCards[i].getValue(), myCards[i].getSuit()
         );
      }

      return output + ")";
   }

   /**
    * Sorts the cards by value.
    */
   public void sort() {
      Card.arraySort(myCards, numCards);
   }
}